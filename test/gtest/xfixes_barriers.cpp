/*

Copyright (c) 2012, Canonical Ltd

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice (including the next
paragraph) shall be included in all copies or substantial portions of the
Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
*/

#include <iostream>
#include <sys/time.h>
#include <unistd.h>
#include <gtest/gtest.h>
#include <xorg/gtest/test.h>
#include <xorg/gtest/environment.h>
#include <X11/Xlib.h>
#include <X11/extensions/XTest.h>
#include <X11/extensions/Xfixes.h>


int main (int argc, char **argv)
{
    ::testing::InitGoogleTest (&argc, argv);
    xorg::testing::Environment* environment = new xorg::testing::Environment ();
    environment->set_conf_file (XORG_DUMMY_CONF);
    environment->set_server (XORG_BINARY);
    testing::AddGlobalTestEnvironment (environment);
    return RUN_ALL_TESTS ();
}

class BarrierTest : public xorg::testing::Test {
    public:
    ::Display *dpy;
    static XErrorEvent *lastError;
    int xtest_eventbase;
    int xtest_errorbase;
    int fixes_eventbase;
    int fixes_errorbase;

    void AssertPointerPosition (int expected_x, int expected_y)
    {
        int x, y, unused_int;
        unsigned int unused_uint;
        Window unused_win;

        XQueryPointer (Display (), DefaultRootWindow (Display ()),
                       &unused_win, &unused_win, &x, &y,
                       &unused_int, &unused_int, &unused_uint);

        ASSERT_TRUE (x == expected_x && y == expected_y) <<
            "Incorrect pointer position: Expected ("<<
            expected_x<< ", "<<expected_y<<"), got "<<
            "("<<x<<", "<<y<<")\n";
    }

    bool WaitForXEvent (int msTimeout = 1000)
    {
        fd_set fds;
        int xfd = ConnectionNumber (Display ());
        struct timeval tv;
        int retval;

        FD_ZERO (&fds);
        FD_SET (xfd, &fds);

        tv.tv_sec = msTimeout / 1000;
        tv.tv_usec = (msTimeout % 1000) * 1000;

        retval = select (xfd + 1, &fds, NULL, NULL, &tv);

        EXPECT_NE (-1, retval)<<"Error waiting for X event";

        return retval;
    }

    protected:
    virtual void SetUp ()
    {
        ASSERT_NO_FATAL_FAILURE (xorg::testing::Test::SetUp());

        dpy = Display ();
        int major = 2, minor = 2;
        ASSERT_TRUE (XTestQueryExtension (dpy,
                                          &xtest_eventbase, &xtest_errorbase,
                                          &major, &minor));
        ASSERT_EQ (2, major);
        ASSERT_TRUE (minor >= 2);

        major = 6;
        minor = 0;
        XFixesQueryVersion (dpy, &major, &minor);
        ASSERT_EQ (6, major);
        ASSERT_TRUE (minor >= 0);

        ASSERT_TRUE (XFixesQueryExtension (dpy,
                                           &fixes_eventbase, &fixes_errorbase));

        lastError = new XErrorEvent;
        XSetErrorHandler (ErrorHandler);
    }

    private:
    static int ErrorHandler (::Display *dpy, XErrorEvent *error)
    {
        memcpy (lastError, error, sizeof (*lastError));
        return 0;
    }
};

XErrorEvent *BarrierTest::lastError = NULL;

TEST_F (BarrierTest, CreateVerticalBarrierSucceeds)
{
    PointerBarrier barrier;
    barrier = XFixesCreatePointerBarrier (dpy, DefaultRootWindow(dpy),
                                          100, 0,
                                          100, 100,
                                          0,
                                          0, NULL);
    ASSERT_NE(None, barrier);
}

TEST_F (BarrierTest, CreateHorizontalBarrierSucceds)
{
    PointerBarrier barrier;
    barrier = XFixesCreatePointerBarrier (dpy, DefaultRootWindow(dpy),
                                          100, 100,
                                          200, 100,
                                          0,
                                          0, NULL);
    ASSERT_NE(None, barrier);
}

TEST_F (BarrierTest, CreateNonAxisAlignedBarrierFails)
{
    XFixesCreatePointerBarrier (dpy, DefaultRootWindow(dpy),
                                0, 0,
                                100, 100,
                                0,
                                0, NULL);
    XSync (Display (), false);
    ASSERT_EQ(BadValue, lastError->error_code);
}

TEST_F (BarrierTest, VerticalBidirectionalBarrierBlocksRelativeMotion)
{
    int barrier_x = 100;
    XFixesCreatePointerBarrier (Display (), DefaultRootWindow (Display ()),
                                barrier_x, 0,
                                barrier_x, 300,
                                0, 0, NULL);

    int x = 200, y = 100, dx = -200, dy = 0;
    XTestFakeMotionEvent (Display (), DefaultScreen (Display ()),
                          x, y, 0);

    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x, y));

    // Relative motion should block on barrier
    XTestFakeRelativeMotionEvent (Display (), dx, dy, 0);

    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (barrier_x, y));
}

TEST_F (BarrierTest, VerticalPositiveXBarrierBlocksMotion)
{
    int barrier_x = 100;
    XFixesCreatePointerBarrier (Display (), DefaultRootWindow (Display ()),
                                barrier_x, 0,
                                barrier_x, 300,
                                BarrierPositiveX, 0, NULL);
    int x = 200, y = 100, dx = -200, dy = 0;
    XTestFakeMotionEvent (Display (), DefaultScreen (Display ()),
                          x, y, 0);

    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x, y));

    // Relative motion in -ve X direction should block on barrier
    XTestFakeRelativeMotionEvent (Display (), dx, dy, 0);

    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (barrier_x, y + dy));

    x = 0, y = 100, dx = 200, dy = 0;
    XTestFakeMotionEvent (Display (), DefaultScreen (Display ()),
                          x, y, 0);

    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x, y));

    // Relative motion in +ve X direction should ignore barrier
    XTestFakeRelativeMotionEvent (Display (), dx, dy, 0);

    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x + dx, y + dy));
}

TEST_F (BarrierTest, VerticalNegativeXBarrierBlocksMotion)
{
    int barrier_x = 100;
    XFixesCreatePointerBarrier (Display (), DefaultRootWindow (Display ()),
                                barrier_x, 0,
                                barrier_x, 300,
                                BarrierNegativeX,
                                0, NULL);

    int x = 200, y = 100, dx = -200, dy = 0;
    XTestFakeMotionEvent (Display (), DefaultScreen (Display ()),
                          x, y, 0);

    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x, y));

    // Relative motion in -ve X direction should ignore barrier
    XTestFakeRelativeMotionEvent (Display (), dx, dy, 0);

    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x + dx, y + dy));

    x = 0, y = 100, dx = 200, dy = 0;
    XTestFakeMotionEvent (Display (), DefaultScreen (Display ()),
                          x, y, 0);

    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x, y));

    // Relative motion in +ve X direction should block on barrier
    XTestFakeRelativeMotionEvent (Display (), dx, dy, 0);

    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (barrier_x - 1, y + dy));
}

TEST_F (BarrierTest, HorizontalBidirectionalBarrierBlocksRelativeMotion)
{
    int barrier_y = 100;
    XFixesCreatePointerBarrier (Display (), DefaultRootWindow (Display ()),
                                0,   barrier_y,
                                300, barrier_y,
                                0, 0, NULL);

    int x = 200, y = 0;
    XTestFakeMotionEvent (Display (), DefaultScreen (Display ()),
                          x, y, 0);

    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x, y));

    // Relative motion in +ve Y direction should block on barrier
    int dx = 0, dy = 200;
    XTestFakeRelativeMotionEvent (Display (), dx, dy, 0);

    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x + dx, barrier_y - 1));

    x = 100, y = 200;
    XTestFakeMotionEvent (Display (), DefaultScreen (Display ()),
                          x, y, 0);

    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x, y));

    // Relative motion in -ve Y direction should block on barrier
    dx = 0, dy = -200;
    XTestFakeRelativeMotionEvent (Display (), dx, dy, 0);

    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x + dx, barrier_y));
}

TEST_F (BarrierTest, HorizontalPositiveYBarrierBlocksMotion)
{
    int barrier_y = 100;
    XFixesCreatePointerBarrier (Display (), DefaultRootWindow (Display ()),
                                0,   barrier_y,
                                300, barrier_y,
                                BarrierPositiveY, 0, NULL);

    int x = 200, y = 0;
    XTestFakeMotionEvent (Display (), DefaultScreen (Display ()),
                          x, y, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x, y));

    // Relative motion in +ve Y direction should ignore barrier
    int dx = 0, dy = 200;
    XTestFakeRelativeMotionEvent (Display (), dx, dy, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x + dx, y + dy));

    x = 100, y = 200;
    XTestFakeMotionEvent (Display (), DefaultScreen (Display ()),
                          x, y, 0);

    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x, y));

    // Relative motion in -ve Y direction should block on barrier
    dx = 0, dy = -200;
    XTestFakeRelativeMotionEvent (Display (), dx, dy, 0);

    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x + dx, barrier_y));
}

TEST_F (BarrierTest, HorizontalNegativeYBarrierBlocksMotion)
{
    int barrier_y = 100;
    XFixesCreatePointerBarrier (Display (), DefaultRootWindow (Display ()),
                                0,   barrier_y,
                                300, barrier_y,
                                BarrierNegativeY, 0, NULL);

    int x = 200, y = 0;
    XTestFakeMotionEvent (Display (), DefaultScreen (Display ()),
                          x, y, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x, y));

    // Relative motion in +ve Y direction should block on barrier
    int dx = 0, dy = 200;
    XTestFakeRelativeMotionEvent (Display (), dx, dy, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x + dx, barrier_y - 1));

    x = 100, y = 200;
    XTestFakeMotionEvent (Display (), DefaultScreen (Display ()),
                          x, y, 0);

    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x, y));

    // Relative motion in -ve Y direction should ignore barrier
    dx = 0, dy = -200;
    XTestFakeRelativeMotionEvent (Display (), dx, dy, 0);

    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x + dx, y + dy));
}

TEST_F (BarrierTest, DestroyPointerBarrierSucceeds)
{
    int barrier_x = 100;
    PointerBarrier barrier;
    barrier = XFixesCreatePointerBarrier (Display (), DefaultRootWindow (Display ()),
                                          barrier_x, 0,
                                          barrier_x, 300,
                                          0, 0, NULL);

    int x = 0, y = 200;
    XTestFakeMotionEvent (Display (), DefaultScreen (Display ()),
                          x, y, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x, y));

    // Check that the barrier exists before we destroy it.
    int dx = 200, dy = 0;
    XTestFakeRelativeMotionEvent (Display (), dx, dy, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (barrier_x - 1, y + dy));

    // Destroy the barrier...
    XFixesDestroyPointerBarrier (Display (), barrier);

    XTestFakeMotionEvent (Display (), DefaultScreen (Display ()),
                          x, y, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x, y));

    // There should be no barrier to block this.
    XTestFakeRelativeMotionEvent (Display (), dx, dy, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x + dx, y + dy));
}

TEST_F (BarrierTest, BarrierIgnoresNonsensicalDirections)
{
    int barrier_x = 100;
    XFixesCreatePointerBarrier (Display (), DefaultRootWindow (Display ()),
                                barrier_x, 0,
                                barrier_x, 300,
                                BarrierPositiveY | BarrierNegativeY,
                                0, NULL);

    int x = 200, y = 100;
    XTestFakeMotionEvent (Display (), DefaultScreen (Display ()),
                          x, y, 0);

    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x, y));

    int dx = -200, dy = 0;
    XTestFakeRelativeMotionEvent (Display (), dx, dy, 0);

    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (barrier_x, y + dy));

    int barrier_y = 100;
    XFixesCreatePointerBarrier (Display (), DefaultRootWindow (Display ()),
                               0,   barrier_y,
                               400, barrier_y,
                               BarrierPositiveX | BarrierNegativeX,
                               0, NULL);

    x = 100, y = 200;
    XTestFakeMotionEvent (Display (), DefaultScreen (Display ()),
                          x, y, 0);

    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x, y));

    dx = 0, dy = -200;
    XTestFakeRelativeMotionEvent (Display (), dx, dy, 0);

    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x + dx, barrier_y));
}

TEST_F (BarrierTest, VerticalBarrierEdges)
{
    int barrier_x = 300, barrier_y1 = 300 , barrier_y2 = 500;
    XFixesCreatePointerBarrier (Display (), DefaultRootWindow (Display ()),
                                barrier_x, barrier_y1,
                                barrier_x, barrier_y2,
                                0, 0, NULL);

    int x = barrier_x + 100, y = barrier_y1 - 1;
    XTestFakeMotionEvent (Display (), DefaultScreen (Display ()),
                          x, y, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x, y));

    // Motion should take us past the top of the barrier...
    int dx = -200, dy = 0;
    XTestFakeRelativeMotionEvent (Display (), dx, dy, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x + dx, y + dy));

    x = barrier_x + 100, y = barrier_y1;
    XTestFakeMotionEvent (Display (), DefaultScreen (Display ()),
                          x, y, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x, y));

    // Motion should hit the top of the barrier...
    dx = -200, dy = 0;
    XTestFakeRelativeMotionEvent (Display (), dx, dy, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (barrier_x, y + dy));

    x = barrier_x + 100, y = barrier_y2;
    XTestFakeMotionEvent (Display (), DefaultScreen (Display ()),
                          x, y, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x, y));

    // Motion should hit the bottom of the barrier...
    dx = -200, dy = 0;
    XTestFakeRelativeMotionEvent (Display (), dx, dy, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (barrier_x, y + dy));

    x = barrier_x + 100, y = barrier_y2 + 1;
    XTestFakeMotionEvent (Display (), DefaultScreen (Display ()),
                          x, y, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x, y));

    // Motion should take us past the bottom of the barrier...
    dx = -200, dy = 0;
    XTestFakeRelativeMotionEvent (Display (), dx, dy, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x + dx, y + dy));
}

TEST_F (BarrierTest, HorizontalBarrierEdges)
{
    int barrier_x1 = 200, barrier_x2 = 500, barrier_y = 300;
    XFixesCreatePointerBarrier (Display (), DefaultRootWindow (Display ()),
                                barrier_x1, barrier_y,
                                barrier_x2, barrier_y,
                                0, 0, NULL);

    int x = barrier_x1 - 1, y = barrier_y - 100;
    XTestFakeMotionEvent (Display (), DefaultScreen (Display ()),
                          x, y, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x, y));

    // Motion should take us past the left edge of the barrier...
    int dx = 0, dy = 200;
    XTestFakeRelativeMotionEvent (Display (), dx, dy, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x + dx, y + dy));

    x = barrier_x1, y = barrier_y - 100;
    XTestFakeMotionEvent (Display (), DefaultScreen (Display ()),
                          x, y, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x, y));

    // Motion should hit the top of the barrier...
    dx = 0, dy = 200;
    XTestFakeRelativeMotionEvent (Display (), dx, dy, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x, barrier_y - 1));

    x = barrier_x2, y = barrier_y - 100;
    XTestFakeMotionEvent (Display (), DefaultScreen (Display ()),
                          x, y, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x, y));

    // Motion should hit the bottom of the barrier...
    dx = 0, dy = 200;
    XTestFakeRelativeMotionEvent (Display (), dx, dy, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x + dx, barrier_y - 1));

    x = barrier_x2 + 1, y = barrier_y - 100;
    XTestFakeMotionEvent (Display (), DefaultScreen (Display ()),
                          x, y, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x, y));

    // Motion should take us past the bottom of the barrier...
    dx = 0, dy = 200;
    XTestFakeRelativeMotionEvent (Display (), dx, dy, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x + dx, y + dy));
}

TEST_F (BarrierTest, CornerBlocksMotion)
{
    int corner_x, corner_y;
    XFixesCreatePointerBarrier (Display (), DefaultRootWindow (Display ()),
                                corner_x, corner_y,
                                corner_x, corner_y + 300,
                                0, 0, NULL);
    XFixesCreatePointerBarrier (Display (), DefaultRootWindow (Display ()),
                                corner_x, corner_y,
                                corner_x + 300, corner_y,
                                0, 0, NULL);

    int x = corner_x + 100, y = corner_y + 100;
    XTestFakeMotionEvent (Display (), DefaultScreen (Display ()),
                          x, y, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x, y));

    XTestFakeRelativeMotionEvent (Display (), -200, -200, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (corner_x, corner_y));
}

TEST_F (BarrierTest, VerticalBarrierWithAdjacentStart)
{
    int barrier_x = 350;
    XFixesCreatePointerBarrier (Display (), DefaultRootWindow (Display ()),
                                barrier_x, 100,
                                barrier_x, 300,
                                0, 0, NULL);

    int x = barrier_x, y = 200;
    XTestFakeMotionEvent (Display (), DefaultScreen (Display ()),
                          x, y, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x, y));

    int dx = -10, dy = 0;
    XTestFakeRelativeMotionEvent (Display (), dx, dy, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (barrier_x, y + dy));

    x = barrier_x, y = 200;
    XTestFakeMotionEvent (Display (), DefaultScreen (Display ()),
                          x, y, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x, y));

    dx = 10, dy = 0;
    XTestFakeRelativeMotionEvent (Display (), dx, dy, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x + dx, y + dy));

    x = barrier_x - 1, y = 200;
    XTestFakeMotionEvent (Display (), DefaultScreen (Display ()),
                          x, y, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x, y));

    dx = 10, dy = 0;
    XTestFakeRelativeMotionEvent (Display (), dx, dy, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (barrier_x - 1, y + dy));

    x = barrier_x - 1, y = 200;
    XTestFakeMotionEvent (Display (), DefaultScreen (Display ()),
                          x, y, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x, y));

    dx = -10, dy = 0;
    XTestFakeRelativeMotionEvent (Display (), dx, dy, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x + dx, y + dy));
}

TEST_F (BarrierTest, HorizontalBarrierWithAdjacentStart)
{
    int barrier_y = 300;
    XFixesCreatePointerBarrier (Display (), DefaultRootWindow (Display ()),
                                100, barrier_y,
                                400, barrier_y,
                                0, 0, NULL);

    int x = 240, y = barrier_y;
    XTestFakeMotionEvent (Display (), DefaultScreen (Display ()),
                          x, y, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x, y));

    int dx = 0, dy = -10;
    XTestFakeRelativeMotionEvent (Display (), dx, dy, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x + dx, barrier_y));

    x = 240, y = barrier_y;
    XTestFakeMotionEvent (Display (), DefaultScreen (Display ()),
                          x, y, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x, y));

    dx = 0, dy = 10;
    XTestFakeRelativeMotionEvent (Display (), dx, dy, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x + dx, y + dy));

    x = 240, y = barrier_y - 1;
    XTestFakeMotionEvent (Display (), DefaultScreen (Display ()),
                          x, y, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x, y));

    dx = 0, dy = 10;
    XTestFakeRelativeMotionEvent (Display (), dx, dy, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x + dx, barrier_y - 1));

    x = 240, y = barrier_y - 1;
    XTestFakeMotionEvent (Display (), DefaultScreen (Display ()),
                          x, y, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x, y));

    dx = 0, dy = -10;
    XTestFakeRelativeMotionEvent (Display (), dx, dy, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x + dx, y + dy));
}

TEST_F (BarrierTest, BarrierNotifyEventFires)
{
    int barrier_y = 300;
    XFixesCreatePointerBarrier (Display (), DefaultRootWindow (Display ()),
                                100, barrier_y,
                                400, barrier_y,
                                0, 0, NULL);

    XFixesSelectBarrierInput (Display (), DefaultRootWindow (Display ()),
                              XFixesBarrierHitNotifyMask);

    int x = 240, y = barrier_y + 50;
    XTestFakeMotionEvent (Display (), DefaultScreen (Display ()),
                          x, y, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x, y));

    XTestFakeRelativeMotionEvent (Display (), 0, -100, 0);

    XFlush (Display ());
    ASSERT_TRUE (WaitForXEvent ())<<"Timed out waiting to receive X event";
    while (XPending (Display ())) {
        XEvent e;
        XNextEvent (Display (), &e);
        switch (e.xany.type - fixes_eventbase) {
            case XFixesBarrierNotify:
                return;
        }
    }
    FAIL () << "Failed to recieve BarrierNotify event";
}

TEST_F (BarrierTest, RecieveOneNotifyEventPerHit)
{
    int barrier_x = 100;
    XFixesCreatePointerBarrier (Display (), DefaultRootWindow (Display ()),
                                barrier_x, 0,
                                barrier_x, 300,
                                0, 0, NULL);

    XFixesSelectBarrierInput (Display (), DefaultRootWindow (Display ()),
                              XFixesBarrierHitNotifyMask);

    int x = 200, y = 100, dx = -200, dy = 0;
    XTestFakeMotionEvent (Display (), DefaultScreen (Display ()),
                          x, y, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x, y));

    /* Generate 5 barrier events */
    for (int i = 0; i < 5; ++i) {
        XTestFakeRelativeMotionEvent (Display (), dx, dy, 0);
    }

    int barrierEventCount = 0;
    XFlush (Display ());
    ASSERT_TRUE (WaitForXEvent ())<<"Timed out waiting to receive X event";
    while (XPending (Display ())) {
        XEvent e;
        XNextEvent (Display (), &e);
        switch (e.xany.type - fixes_eventbase) {
            case XFixesBarrierNotify:
                barrierEventCount++;
                break;
        }
    }
    ASSERT_EQ (5, barrierEventCount);
}

TEST_F (BarrierTest, BarrierEventHasNonZeroVelocity)
{
    int barrier_x = 100;
    XFixesCreatePointerBarrier (Display (), DefaultRootWindow (Display ()),
                                barrier_x, 0,
                                barrier_x, 300,
                                0, 0, NULL);

    XFixesSelectBarrierInput (Display (), DefaultRootWindow (Display ()),
                              XFixesBarrierHitNotifyMask);

    int x = 200, y = 100, dx = -200, dy = 0;
    XTestFakeMotionEvent (Display (), DefaultScreen (Display ()),
                          x, y, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x, y));

    /* One relative event to ensure the server has a non-zero
     * last-event-time */
    XTestFakeRelativeMotionEvent (Display (), 10, 10, 0);
    /* Run the pointer into the barrier */
    XTestFakeRelativeMotionEvent (Display (), dx, dy, 0);

    XFlush (Display ());
    ASSERT_TRUE (WaitForXEvent ())<<"Timed out waiting to receive X event";
    while (XPending (Display ())) {
        XEvent e;
        XNextEvent (Display (), &e);
        switch (e.xany.type - fixes_eventbase) {
            case XFixesBarrierNotify:
                XFixesBarrierNotifyEvent *notify = (XFixesBarrierNotifyEvent *)&e;
                ASSERT_LT (0, notify->velocity);
                return;
        }
    }
    FAIL () << "Failed to receive barrier event";
}

TEST_F (BarrierTest, ScreenEdgeVerticalBarrierEventHasNonZeroVelocity)
{
    int barrier_x = 0;
    XFixesCreatePointerBarrier (Display (), DefaultRootWindow (Display ()),
                                barrier_x, 0,
                                barrier_x, 300,
                                0, 0, NULL);

    XFixesSelectBarrierInput (Display (), DefaultRootWindow (Display ()),
                              XFixesBarrierHitNotifyMask);

    int x = 100, y = 100, dx = -200, dy = 0;
    XTestFakeMotionEvent (Display (), DefaultScreen (Display ()),
                          x, y, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x, y));

    /* One relative event to ensure the server has a non-zero
     * last-event-time */
    XTestFakeRelativeMotionEvent (Display (), 10, 10, 0);
    /* Run the pointer into the barrier */
    XTestFakeRelativeMotionEvent (Display (), dx, dy, 0);

    XFlush (Display ());
    ASSERT_TRUE (WaitForXEvent ())<<"Timed out waiting to receive X event";
    while (XPending (Display ())) {
        XEvent e;
        XNextEvent (Display (), &e);
        switch (e.xany.type - fixes_eventbase) {
            case XFixesBarrierNotify:
                XFixesBarrierNotifyEvent *notify = (XFixesBarrierNotifyEvent *)&e;
                ASSERT_LT (0, notify->velocity);
                return;
        }
    }
    FAIL () << "Failed to receive barrier event";
}

TEST_F (BarrierTest, ScreenEdgeHorizontalBarrierEventHasNonZeroVelocity)
{
    int barrier_y = 0;
    XFixesCreatePointerBarrier (Display (), DefaultRootWindow (Display ()),
                                0, barrier_y,
                                300, barrier_y,
                                0, 0, NULL);

    XFixesSelectBarrierInput (Display (), DefaultRootWindow (Display ()),
                              XFixesBarrierHitNotifyMask);

    int x = 100, y = 100, dx = 0, dy = -200;
    XTestFakeMotionEvent (Display (), DefaultScreen (Display ()),
                          x, y, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x, y));

    /* One relative event to ensure the server has a non-zero
     * last-event-time */
    XTestFakeRelativeMotionEvent (Display (), 10, 10, 0);
    /* Run the pointer into the barrier */
    XTestFakeRelativeMotionEvent (Display (), dx, dy, 0);

    XFlush (Display ());
    ASSERT_TRUE (WaitForXEvent ())<<"Timed out waiting to receive X event";
    while (XPending (Display ())) {
        XEvent e;
        XNextEvent (Display (), &e);
        switch (e.xany.type - fixes_eventbase) {
            case XFixesBarrierNotify:
                XFixesBarrierNotifyEvent *notify = (XFixesBarrierNotifyEvent *)&e;
                ASSERT_LT (0, notify->velocity);
                return;
        }
    }
    FAIL () << "Failed to receive barrier event";
}

TEST_F (BarrierTest, ReceiveOneBarrierEventPerHitOnScreenEdge)
{
    int barrier_x = 0;
    XFixesCreatePointerBarrier (Display (), DefaultRootWindow (Display ()),
                                barrier_x, 0,
                                barrier_x, 300,
                                0, 0, NULL);

    XFixesSelectBarrierInput (Display (), DefaultRootWindow (Display ()),
                              XFixesBarrierHitNotifyMask);

    int x = 20, y = 100, dx = -40, dy = 0;
    XTestFakeMotionEvent (Display (), DefaultScreen (Display ()),
                          x, y, 0);
    ASSERT_NO_FATAL_FAILURE (AssertPointerPosition (x, y));

    /* Generate 5 barrier events */
    for (int i = 0; i < 5; ++i) {
        XTestFakeRelativeMotionEvent (Display (), dx, dy, 0);
    }

    int barrierEventCount = 0;
    XFlush (Display ());
    ASSERT_TRUE (WaitForXEvent ())<<"Timed out waiting to receive X event";
    while (XPending (Display ())) {
        XEvent e;
        XNextEvent (Display (), &e);
        switch (e.xany.type - fixes_eventbase) {
            case XFixesBarrierNotify:
                barrierEventCount++;
                break;
        }
    }
    ASSERT_EQ (5, barrierEventCount);
}
